package com.example.myapplication101

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.SeekBar
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        val initialTextViewTranlationY = textView_numberDisplay.translationY

        seekBar.setOnSeekBarChangeListener(object: SeekBar.OnSeekBarChangeListener {
            override fun onProgressChanged(seekBar: SeekBar?, progress: Int, fromUser: Boolean) {
                textView_numberDisplay.text = progress.toString()

                val translationDistance = (initialTextViewTranlationY
                        + progress * resources.getDimension(R.dimen.text_anim_step) * -1)

                textView_numberDisplay.animate().translationY(translationDistance)
if (!fromUser){
    textView_numberDisplay.animate().setDuration(500)
        .rotationBy(360f).translationY(initialTextViewTranlationY)
}

            }

            override fun onStartTrackingTouch(seekBar: SeekBar?) {

            }

            override fun onStopTrackingTouch(seekBar: SeekBar?) {
            }
        })
        button_reset.setOnClickListener { v ->
            seekBar.progress = 0;

        }
    }
}
