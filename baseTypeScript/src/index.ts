import * as functions from "firebase-functions";
import * as admin from "firebase-admin";

interface UserListInterface {
  usuarios: {
    id: string;
    nome: string;
    observacoes?: any;
    profissao: string;
    sexo: string;
    emailPrincipal: string;
    emailAuxiliar: string;
    telefone: string;
    endereco_cidade: string;
    endereco_uf: string;
    endereco_pais: string;
    cargo: string;
    filhos: string;
    updateBy: string;
    updateDate: string;
  }[];
}

const serviceAccount = require("./firebaseKey.json");
const userList: UserListInterface = require("./usuarios.json");
let arrConsertado = [];

admin.initializeApp({
  credential: admin.credential.cert(serviceAccount),
  databaseURL: "https://arteconecta-27292.firebaseio.com"
});

console.log("Funcionou", userList.usuarios[0]);

userList.usuarios.forEach((element, key) => {
  /* if (key < 10) { */
    console.log(
      key +
        "-" +
        Math.random()
          .toString(36)
          .substr(2, 9)
    );
    let hashId = Math.random()
      .toString(36)
      .substr(2, 9);
      element.id = null;
      element.id = hashId;
    const arrUserListPath = admin.database().ref("/usuarios/" + element.id);
    arrUserListPath.set(element);
});
