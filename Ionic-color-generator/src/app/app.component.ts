import { Component } from '@angular/core';
import * as Color from 'color';
import { NgForm } from '@angular/forms';
@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss'],
})
export class AppComponent {
  title = 'color-genearator';
  hexadecimal = '#008abc';
  corNome = 'pob-blue';
  colorArr = [];
  colorFullArrRoot = [];
  colorFullArrClass = [];
  colorFullTextRoot = '';
  colorFullTextClass = '';
  constructor() {
    console.log(Color('white'));
  }

  changeStyle(color, addList?: boolean) {
    if (addList) {
      if (this.hexadecimal.length >= 6) {
        return {
          'background-color': color,
          color: this.contrast(color),
        };
      } else {
        return {
          'background-color': '#ffffff',
          color: '#000000',
        };
      }
    } else {
      return {
        'background-color': color,
        color: this.contrast(color),
      };
    }
  }

  removeColor(position: number) {
    console.log(position);
    this.colorArr.forEach((element, i) => {
      if (element.id === position) {
        this.colorArr.splice(i, 1);
      }
    });
    console.log(this.colorArr);
  }

  contrast(color: string, ratio = 0.8) {
    /* const ratio ; */
    const retorno = Color(color).isDark() ? Color('white') : Color('black');

    return retorno.rgb().hex();
  }

  addToArray(form: NgForm) {
    if (form.valid) {
      const colorObj = {
        id: this.colorArr.length,
        nome: this.corNome,
        hexadecimal: this.hexadecimal,
      };
      this.colorArr.push(colorObj);

      console.log(this.colorArr);
    }
  }

  createIonicColors() {
    this.colorFullArrRoot = [];
    this.colorFullTextRoot = '';
    this.colorFullArrClass = [];
    this.colorFullTextClass = '';
    this.colorArr.forEach((element, index) => {
      this.formatRGB(element.hexadecimal);
      this.colorFullTextRoot =
        this.colorFullTextRoot +
        `
        --ion-color-${element.nome}: ${element.hexadecimal};
        --ion-color-${element.nome}-rgb: ${this.formatRGB(element.hexadecimal)};
        --ion-color-${element.nome}-contrast: ${this.contrast(
          element.hexadecimal,
          0.1
        )};
        --ion-color-${element.nome}-contrast-rgb: ${this.formatRGB(
          this.contrast(element.hexadecimal, 0.1)
        )};
        --ion-color-${element.nome}-shade: ${Color(element.hexadecimal)
          .darken(0.1)
          .hex()};
        --ion-color-${element.nome}-tint: ${Color(element.hexadecimal)
          .lighten(0.1)
          .hex()};
        |;
        |;
        |;
          `;

      this.colorFullTextClass =
        this.colorFullTextClass +
        `
          .ion-color-${element.nome} {

            --ion-color-base: var(--ion-color-${element.nome});
            --ion-color-base-rgb: var(--ion-color-${element.nome}-rgb);
            --ion-color-contrast: var(--ion-color-${element.nome}-contrast);
            --ion-color-contrast-rgb: var(--ion-color-${element.nome}-contrast-rgb);
            --ion-color-shade: var(--ion-color-${element.nome}-shade);
            --ion-color-tint: var(--ion-color-${element.nome}-tint);

          }
          |;
          |;
          |;

          `;
    });
    this.colorFullArrClass = this.colorFullTextClass.split(';');
    this.colorFullArrRoot = this.colorFullTextRoot.split(';');
    console.log(this.colorFullTextRoot);
    this.colorFullArrRoot.forEach((element: string) => {
      element = element.replace(/\s+/, '');
    });
    console.log('O que há aqui?', this.colorFullArrRoot);
  }

  formatRGB(rgbString: string) {
    const corTransformada = Color(rgbString).rgb().toString();
    let recorte = corTransformada.substring(3, corTransformada.length);
    recorte = recorte.replace('(', '');
    recorte = recorte.replace(')', '');

    console.log('5454', recorte);
    return recorte;
  }
}
