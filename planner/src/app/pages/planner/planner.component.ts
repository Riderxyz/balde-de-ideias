import { Component } from '@angular/core';
import {
  CdkDragDrop,
  moveItemInArray,
  transferArrayItem,
} from '@angular/cdk/drag-drop';

@Component({
  templateUrl: './planner.component.html',
  styleUrls: ['./planner.component.scss'],
})
export class PlannerComponent {
  todo = ['Get to work', 'Pick up groceries', 'Go home', 'Fall asleep'];

  done = ['Get up', 'Brush teeth', 'Take a shower', 'Check e-mail', 'Walk dog'];

  diasSemanaArr = [
    {
      nome: 'Domingo',
      tarefas:[{
        nome:'teste'
      }]
    },
    {
      nome: 'Segunda',
      tarefas:[
        {
          nome:'teste'
        }
      ]
    },
    {
      nome: 'Terça',
      tarefas:[
        {
          nome:'teste'
        }
      ]
    },
    {
      nome: 'Quarta',
      tarefas:[
        {
          nome:'teste'
        }
      ]
    },
    {
      nome: 'Quinta',
      tarefas:[{
        nome:'teste'
      }
    ]
    },
    {
      nome: 'Sexta',
      tarefas:[
        {
          nome:'teste'
        }
      ]
    },
    {
      nome: 'Sabado',
      tarefas:[{
        nome:'teste'
      }]
    },
  ];

  drop(event: CdkDragDrop<string[]>): void {
    if (event.previousContainer === event.container) {
      moveItemInArray(
        event.container.data,
        event.previousIndex,
        event.currentIndex
      );
    } else {
      transferArrayItem(
        event.previousContainer.data,
        event.container.data,
        event.previousIndex,
        event.currentIndex
      );
    }
  }
}
