import { PlannerComponent } from './pages/planner/planner.component';
import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

const routes: Routes = [
  { path: 'planner', component: PlannerComponent },
  { path: '', pathMatch: 'full', redirectTo: 'planner' },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
