// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false,
  firebase:{
    apiKey: "AIzaSyCU-oQ19D_UtUNkz9iQNfTJQByc8R2a61w",
    authDomain: "graal-fb3f5.firebaseapp.com",
    databaseURL: "https://graal-fb3f5.firebaseio.com",
    projectId: "graal-fb3f5",
    storageBucket: "graal-fb3f5.appspot.com",
    messagingSenderId: "243499474605",
    appId: "1:243499474605:web:460c9fcbd4f786b8367024",
    measurementId: "G-K9MS8C7JJ0"
  }
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
