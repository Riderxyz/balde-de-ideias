import { Component, OnInit } from '@angular/core';
import { ParticlesConfig } from './particles-config';
declare let particlesJS: any;

@Component({
  templateUrl: './login.page.html',
  styleUrls: ['./login.page.scss']
})
export class LoginPage implements OnInit {
  myStyle: object = {};
  myParams: object = {};
  width = 100;
  height = 100;
  constructor() { }

  ngOnInit(): void {
    this.invokeParticles();

    this.myStyle = {
      width: '100%',
      height: '100%',
      'z-index': -1,
      top: 0,
      left: 0,
      right: 0,
      bottom: 0
    };

    this.myParams = {
      particles: {
        number: {
          value: 400
        },
        color: {
          value: '#00ffff'
        },
        opacity:{
          value:0.5
        },
        size: {
          value: 5,
          random: true,
          anim: {
            enable: false,
            speed: 80,
            size_min: 0.1,
            sync: false
          }
        },
        move: {
          speed: 2
        }
      }
    };
  }
  public invokeParticles(): void {
    particlesJS('particles-js', ParticlesConfig, function() {});
  }
}
