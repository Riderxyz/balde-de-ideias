import { NgModule, Component } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { HomePage } from './home/home.page';

import { MaterialModule } from '../material.module';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { ComponentsModule } from '../components/components.module';
import { LoginPage } from './login/login.page';

const routes: Routes = [
  {
    path: '',
    redirectTo: '/home',
    pathMatch: 'full'
  },
  { path: 'home', component: HomePage },
  { path: 'login', component: LoginPage }
];

@NgModule({
  imports: [
    RouterModule.forRoot(routes),
    MaterialModule,
    ComponentsModule,
    CommonModule,
    FormsModule
  ],
  exports: [RouterModule, HomePage],
  declarations: [HomePage, LoginPage],

})
export class PagesModule { }
