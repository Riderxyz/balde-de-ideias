import { Component, OnInit } from '@angular/core';
import { CentralRxJsService } from './services/centralRXJS.service';
import moment from 'moment';
import { config } from './services/config'
import { MatIconRegistry } from '@angular/material/icon';
import { DomSanitizer } from "@angular/platform-browser";
@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent implements OnInit {
  title = 'graalRj';
  activatedTheme: string = 'purpleish-theme';
  constructor(
    private centralRXJS: CentralRxJsService,
    private matIconRegistry: MatIconRegistry,
    private domSanitizer: DomSanitizer
  ) {
    this.matIconRegistry.addSvgIcon(
      "graalLogo",
      this.domSanitizer.bypassSecurityTrustResourceUrl("../assets/icons/logos/graal-Logo.svg")
    );
  }
  ngOnInit() {
    this.centralRXJS.DataToReceive.subscribe((res) => {
      if (res.key === config.rxjsCentralKeys.changeTheme) {
        this.activatedTheme = res.data;
      }
    })
    moment.locale('pt-br');
    
  }

}
