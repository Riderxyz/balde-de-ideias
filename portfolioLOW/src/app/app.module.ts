import { NgModule, NO_ERRORS_SCHEMA } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { ReactiveFormsModule, FormsModule } from '@angular/forms';

import { AppComponent } from './app.component';
import { MDBBootstrapModule } from 'angular-bootstrap-md';

// AngularFire
import { AngularFireModule } from '@angular/fire';
import { AngularFireDatabaseModule } from '@angular/fire/database';
import { AngularFireAuthModule } from '@angular/fire/auth';
import { AngularFireMessagingModule } from '@angular/fire/messaging';
import { AngularFireStorageModule } from '@angular/fire/storage';
import { AngularFirePerformanceModule } from '@angular/fire/performance';

import { environment } from '../environments/environment';
import { ServiceWorkerModule } from '@angular/service-worker';

// Modules
import { HttpClientModule } from '@angular/common/http';
import { PagesModule } from './pages/page.module';
import { MaterialModule } from './material.module';
import { ComponentsModule } from './components/components.module';
//  Service
import { CentralRxJsService } from './services/centralRXJS.service';
import { ToastService } from './services/toast.service';
import { DataService } from './services/data.service';

const AngularFire = [
  AngularFireModule.initializeApp(environment.firebase),
  AngularFireDatabaseModule,
  AngularFireAuthModule,
  AngularFireMessagingModule,
  AngularFireStorageModule,
  AngularFirePerformanceModule
];
const FormModules = [ReactiveFormsModule, FormsModule];

@NgModule({
  declarations: [AppComponent],
  imports: [
    HttpClientModule,
    MDBBootstrapModule.forRoot(),
    MaterialModule,
    BrowserModule,
    BrowserAnimationsModule,
    ...FormModules,
    PagesModule,
    ComponentsModule,
    // AngularFire
    ...AngularFire,
    ServiceWorkerModule.register('combined-sw.js', {
      enabled: environment.production
    })
  ],
  providers: [CentralRxJsService, ToastService, DataService],
  bootstrap: [AppComponent]
})
export class AppModule {}
