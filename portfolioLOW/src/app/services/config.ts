
export const config = {
  rxjsCentralKeys: {
    GridResize: 'gridResize',
    GridReady: 'gridReady',
    ChangeToMobile: 'changeToMobile',
    ChangeToWeb: 'changeToWeb',
    pwaUpdateRequest: 'pwaUpdateRequest',
    lastUpdate: 'lastUpdate',
    openSideMenu: 'openSideMenu',
    closeSideMenu: 'closeSideMenu',
    onGridFilter: 'onGridFilter',
    changeTheme: 'changeTheme'
  },
  getRandomId: () => {
    return Math.random()
      .toString(36)
      .substr(2, 9);
  },
  url: {
    base: '/ArteConecta',
    dbUser: '/usuario',
    dbMateriais: '/material',
    dbObjetivos: '/objetivo',
    dbAtividades: '/atividades',
    pushNotificationAPI:
      'https://us-central1-arteconecta-27292.cloudfunctions.net/SendNotification'
  },
};
