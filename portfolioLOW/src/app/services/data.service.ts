import { Injectable } from "@angular/core";
import { AngularFireDatabase } from "@angular/fire/database";
import { config } from "./config";

import { AngularFireMessaging } from "@angular/fire/messaging";
import { ToastService } from "./toast.service";
@Injectable()
export class DataService {
  constructor(
    public db: AngularFireDatabase,
    private afMessaging: AngularFireMessaging,
    private toastSrv: ToastService
  ) { }

  public saveTo(ev: any, path: string) {
    this.db
      .list(config.url.base + path)
      .set(ev.id, ev)
      .then(res => {
        switch (path) {
          case config.url.dbAtividades:
            this.toastSrv.showToastSucess("Atividade incluida com sucesso");
            break;
          case config.url.dbUser:
            this.toastSrv.showToastSucess("Usuario incluido com sucesso");
            break;
          case config.url.dbMateriais:
            this.toastSrv.showToastSucess("Material incluido com sucesso");
            break;
          case config.url.dbObjetivos:
            this.toastSrv.showToastSucess("Objetivo incluido com sucesso");
            break;
        }
      })
      .catch(err => {
        this.toastSrv.showToastError(
          "Não foi possivel concluir a sua operação",
          "por favor, tente novamente"
        );
      });
  }
  set deleteFromDB(ev: { arr: any[]; path: string }) {
    ev.arr.forEach(element => {
      this.db.list(config.url.base + ev.path).remove(element.id);
    });
  }

  public deleteFolderFromDB(url: string) {
    this.db.list(url).remove();
  }

  sendNotification() { }
}
