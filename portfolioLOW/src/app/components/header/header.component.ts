import { Component, OnInit } from '@angular/core';
import { CentralRxJsService } from 'src/app/services/centralRXJS.service';
import { config } from 'src/app/services/config';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.scss']
})
export class HeaderComponent implements OnInit {
activeTheme = 'purpleish-theme'
  constructor(
    private centralRXJS: CentralRxJsService
  ) { }

  ngOnInit(): void {
  }

  toggleDarkTheme(ev:boolean){
    console.log(ev);
    if (ev) {
      this.activeTheme = 'dark-theme'
    } else{
      this.activeTheme = 'purpleish-theme'
    }
    this.centralRXJS.sendData = {
      key:config.rxjsCentralKeys.changeTheme,
      data: this.activeTheme
    }
  }
}
