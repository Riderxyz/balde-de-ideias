import { BrowserModule } from '@angular/platform-browser';
import { NgModule, NO_ERRORS_SCHEMA, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { RouteReuseStrategy } from '@angular/router';

import { IonicModule, IonicRouteStrategy } from '@ionic/angular';
import { MDBBootstrapModule } from 'angular-bootstrap-md';

import { AngularFireModule } from '@angular/fire';
import { AngularFireDatabaseModule } from '@angular/fire/database';
import { AngularFireAuthModule } from '@angular/fire/auth';
import { AngularFireMessagingModule } from '@angular/fire/messaging';
import { environment } from '../environments/environment';
import { ServiceWorkerModule } from '@angular/service-worker';

import { HomeComponent } from '../pages/home/home.component';

import { SnackbarComponent } from '../components/snackbar/snackbar.component';
import { HeaderComponent } from '../components/header/header.component';
import { ModalIncluirTimeComponent } from 'src/components/modal-incluirTime/modal-incluirTime.component';

import { PwaService } from 'src/services/pwa.service';
import { LoginService } from 'src/services/login.service';
import { DataService } from 'src/services/data.service';
import { HttpClientModule } from '@angular/common/http';

const AngularFire = [
  AngularFireModule.initializeApp(environment.firebase),
  AngularFireDatabaseModule,
  AngularFireAuthModule,
  AngularFireMessagingModule
];

@NgModule({
  declarations: [
    AppComponent,
    HomeComponent,
    SnackbarComponent,
    HeaderComponent,
    ModalIncluirTimeComponent
  ],
      entryComponents: [
        SnackbarComponent,
        HeaderComponent,
        ModalIncluirTimeComponent
      ],
  imports: [
    BrowserModule,
    BrowserAnimationsModule,
    FormsModule,
    AppRoutingModule,
    MDBBootstrapModule.forRoot(),
    IonicModule.forRoot(),
    ReactiveFormsModule,
    HttpClientModule,
    ...AngularFire,
    ServiceWorkerModule.register('combined-sw.js', { enabled: environment.production })
  ],
  providers: [
    PwaService,
    DataService,
    LoginService,
    { provide: RouteReuseStrategy, useClass: IonicRouteStrategy }
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
