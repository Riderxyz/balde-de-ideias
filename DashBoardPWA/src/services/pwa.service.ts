import { Injectable } from '@angular/core';
import { SwUpdate } from '@angular/service-worker';
import { ToastController } from '@ionic/angular';
@Injectable()
export class PwaService {
  constructor(
    private swUpdate: SwUpdate,
    private toastController: ToastController
  ) {
    swUpdate.available.subscribe(event => {
      this.SwUpdateToast();
    });
  }
  async SwUpdateToast() {
    const toast = await this.toastController.create({
      message: 'Uma nova versão esta disponivel',
      duration: 0,
      cssClass: 'upgrade-version-pwa-snackBar',
      buttons: [
        {
          side: 'start',
          icon: 'sync',
          text: 'Atualizar',
          cssClass: 'toast-button-fav',
          handler: () => {
            console.log('Favorite clicked');
            window.location.reload();
          }
        },
        {
          side: 'end',
          icon: 'close',
          text: 'Fechar',
          cssClass: 'toast-button-fav',
          handler: () => {
            console.log('Favorite clicked');
          }
        }]
    });
    toast.present();
  }
}

