import { Component, OnInit } from '@angular/core';
import { MDBModalRef } from 'angular-bootstrap-md';

@Component({
  selector: 'app-modal-incluirTime',
  templateUrl: './modal-incluirTime.component.html',
  styleUrls: ['./modal-incluirTime.component.scss']
})
export class ModalIncluirTimeComponent implements OnInit {

  constructor(private mdbModal: MDBModalRef) { }
  elo = []
  positionInGame = [

  ]
  ngOnInit() {
    this.startEloArr()
  }

  startEloArr() {
    this.elo = [
      {
        name: 'Bronze',
        value: 'bronze'
      },
      {
        name: 'Prata',
        value: 'prata'
      },
      {
        name: 'Ouro',
        value: 'ouro'
      },
      {
        name: 'Platina',
        value: 'platina'
      },
      {
        name: 'Diamante',
        value: 'diamente'
      },
      {
        name: 'Mestre',
        value: 'mestre'
      },
      {
        name: 'Desafiante',
        value: 'desafiante'
      }
    ]
    /*    this.elo = [
      {
        name: 'bronze 5',
        value: 'bronze 5'
      }, {
        name: 'bronze 4',
        value: 'bronze 4'
      },
      {
        name: 'bronze 3',
        value: 'bronze 3'
      },
      {
        name: 'bronze 2',
        value: 'bronze 2'
      },
      {
        name: 'bronze 1',
        value: 'bronze 1'
      },
      {
        name: 'prata 5',
        value: 'prata 5'
      }, {
        name: 'prata 4',
        value: 'prata 4'
      },
      {
        name: 'prata 3',
        value: 'prata 3'
      },
      {
        name: 'prata 2',
        value: 'prata 2'
      },
      {
        name: 'prata 1',
        value: 'prata 1'
      },
      {
        name: 'Ouro 5',
        value: 'Ouro 5'
      },
      {
        name: 'Ouro 4',
        value: 'Ouro 4'
      }, {
        name: 'Ouro 3',
        value: 'Ouro 3'
      },
      {
        name: 'Ouro 2',
        value: 'Ouro 2'
      },
      {
        name: 'Ouro 1',
        value: 'Ouro 1'
      },
      {
        name: 'Platina 5',
        value: 'Platina 5'
      },
      {
        name: 'Platina 4',
        value: 'Platina 4'
      },
      {
        name: 'Platina 3',
        value: 'Platina 3'
      },
      {
        name: 'Platina 2',
        value: 'Platina 2'
      },
      {
        name: 'Platina 1',
        value: 'Platina 1'
      },
      {
        name: 'Diamante 5',
        value: 'Diamante 5'
      },
      {
        name: 'Diamante 4',
        value: 'Diamante 4'
      },
      {
        name: 'Diamante 4',
        value: 'Diamante 4'
      },
      {
        name: 'Diamante 3',
        value: 'Diamante 3'
      },

      {
        name: 'Diamante 2',
        value: 'Diamante 2'
      },
      {
        name: 'Diamante 1',
        value: 'Diamante 1'
      },
      {
        name: 'Mestre',
        value: 'Mestre'
      },
      {
        name: 'Desafiante',
        value: 'Desafiante'
      }] */


  }

}
