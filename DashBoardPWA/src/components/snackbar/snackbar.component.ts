import { Component, OnInit, Input, OnChanges } from '@angular/core';
import { animationsSnackBar } from './snackbar.animation';

@Component({
  selector: 'app-snackbar',
  templateUrl: './snackbar.component.html',
  styleUrls: ['./snackbar.component.scss'],
  animations: animationsSnackBar
})
export class SnackbarComponent implements OnInit, OnChanges {
 showSnackBar = true;
  @Input() text = '';
  @Input() getSnack = false;
  state = 'final';
  constructor() { }

  ngOnInit() {
  }

  ngOnChanges(ev) {
    console.log('mudei ', ev);
  //  this.changeState();
  this.showSnackBar = !this.showSnackBar;
  }
  changeState() {
    if (!this.showSnackBar) {
      this.state = 'initial';
    } else {
      this.state = 'final';
    }
  }

}
