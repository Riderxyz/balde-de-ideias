import { Component, OnInit } from '@angular/core';
import { AngularFireMessaging } from '@angular/fire/messaging';
import * as firebase from 'firebase';
import { ToastController } from '@ionic/angular';
import { MDBModalService, MDBModalRef } from 'angular-bootstrap-md';
import { ModalIncluirTimeComponent } from 'src/components/modal-incluirTime/modal-incluirTime.component';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss']
})
export class HomeComponent implements OnInit {
  cardArr: {
    icon: string;
    name: string;
    value: string;
    progressBar: string;
    class: string;
  }[] = [];
  messaging = null;
  possibleToken = '';
  showSnackBar = false;
  modalRef: MDBModalRef;
  constructor(
    private afMessaging: AngularFireMessaging,
    public toastController: ToastController,
    private modalService: MDBModalService) { }

  ngOnInit(): void {
    this.cardArr = [
      {
        icon: 'far fa-money-bill-alt',
        name: 'SALES',
        value: 'R$2000',
        progressBar: '25',
        class: 'primary-color'
      },
      {
        icon: 'fas fa-chart-line',
        name: 'SUBSCRIPTIONS',
        value: '200',
        progressBar: '25',
        class: 'warning-color'
      },
      {
        icon: 'fas fa-chart-pie',
        name: 'TRAFFIC',
        value: '20000',
        progressBar: '75',
        class: 'light-blue lighten-1'
      },
      {
        icon: 'far fa-chart-bar',
        name: 'ORGANIC TRAFFIC',
        value: '2000',
        progressBar: '55',
        class: 'red accent-2'
      },
    ];
  }

  requestPushNotificationsPermission() {
    this.afMessaging.requestToken
      .subscribe(
        (token) => {
          console.log('Permission granted! Save to the server!', token);
        },
        (error) => {
          console.error(error);
        }
      );
  }

  async presentToast() {
    const toast = await this.toastController.create({
      message: 'Uma nova versão esta disponivel',
      duration: 0,
      cssClass: 'upgrade-version-pwa-snackBar',
      buttons: [
        {
          side: 'start',
          icon: 'sync',
          text: 'Atualizar',
          cssClass: 'toast-button-fav',
          handler: () => {
            console.log('Favorite clicked');
            window.location.reload();
          }
        },
        {
          side: 'end',
          icon: 'close',
          text: 'Fechar',
          cssClass: 'toast-button-fav',
          handler: () => {
            console.log('Favorite clicked');
          }
        }]
    });
    toast.present();
  }

  showModal() {
    this.modalRef = this.modalService.show(ModalIncluirTimeComponent, {
      class:'modal-form-marginTop',

    })
  }

}
