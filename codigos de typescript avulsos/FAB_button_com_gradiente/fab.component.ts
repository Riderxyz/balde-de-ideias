import { Component, OnInit } from '@angular/core';
import { FabListInterface } from './fabList.model';

@Component({
    selector: 'selector-name',
    templateUrl: 'fab.component.html',
    styleUrls: ['./fab.component.scss']
})

export class FABComponent implements OnInit {
    FabList = [];
    constructor() { }

    ngOnInit() {
        this.FabList = this.FabButtonList;
     }

    public get FabButtonList() {
        const arrFab: FabListInterface[] = [
          {
          cor: 'info',
          icon: 'fa fa-search',
          comando: 'insira o comando que ele ira fazer aqui',
          tooltip: 'Buscar'
        },
        {
          cor: 'warning',
          icon: 'fas fa-clock',
          comando: 'insira o comando que ele ira fazer aqui',
          tooltip: 'DashBoard'
        },
        {
          cor: 'success',
          icon: 'fas fa-table',
          comando: 'insira o comando que ele ira fazer aqui',
          tooltip: 'Gerenciamento Diario'
        },
        {
          cor: 'danger',
          icon: 'fas fa-user',
          comando: 'insira o comando que ele ira fazer aqui',
          tooltip: 'Log in'
        }
    
        ]
        return arrFab;
      }

    GoTo(ev) {
        console.log(ev);
       /*  switch (ev.comando) {
          case config.fabCommand.FiltrarGrid:
            this.showFab = false;
            if (this.route.url === '/dashboard') {
              const comando = config.rxjsCentralKeys.ShowFilterEsteiraDashBoard;
              this.centralRx.sendData = comando;
            } else {
              alert('Ainda será implementado');
            }
            break;
          case config.fabCommand.GoToDash:
            this.showFab = false;
            this.route.navigateByUrl('/dashboard');
            break;
          case config.fabCommand.GoToGD:
            this.showFab = false;
            this.route.navigateByUrl('/gdboard');
            break;
          case config.fabCommand.GoToUser:
            this.showLogin();
            this.showFab = false;
            break;
          default:
            break;
        } */
    
      }
}