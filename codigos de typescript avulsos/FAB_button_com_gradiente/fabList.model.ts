export interface FabListInterface {
  cor: 'primary' | 'success' | 'info' | 'warning' | 'danger';
  icon: string;
  comando: any;
  tooltip: string;
}
