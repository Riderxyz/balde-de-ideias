@ECHO OFF
CLS
ECHO 1.Nebular + Firebase
ECHO 2.PrimeNg + Firebase
ECHO 3.AngularMaterial + Firebase
ECHO 4.Nebular + Firebase + PWA
ECHO 5.PrimeNg + Firebase + PWA
ECHO 5.AngularMaterial + Firebase + PWA
ECHO.


CHOICE /C 123456 /M "Enter your choice:"

:: Note - list ERRORLEVELS in decreasing order
IF ERRORLEVEL 5 GOTO SwitchUser
IF ERRORLEVEL 4 GOTO Logoff
IF ERRORLEVEL 3 GOTO CloseAllWindows
IF ERRORLEVEL 2 GOTO PrimeNgFirebase
IF ERRORLEVEL 1 GOTO NebularFirebase

:NebularFirebase
ECHO Nebular + Firebase escolhido. Iniciando processo de instalação
npm i firebase @angular/fire @angular/animations @fortawesome/fontawesome-free @nebular/moment @nebular/theme@3.6.2  eva-icons moment nebular-icons ng2-completer angular-animations
GOTO End

:PrimeNgFirebase
ECHO PrimeNg + Firebase escolhido. Iniciando processo de instalação
npm i firebase @angular/fire @angular/animations @fortawesome/fontawesome-free primeng primeicons angular-animations

GOTO End

:CloseAllWindows
ECHO Close All Windows (put your close all windows code here)
GOTO End

:Logoff
ECHO Logoff (put your log off code here)
GOTO End

:SwitchUser
ECHO Switch User (put your switch user code here)
GOTO End

:End