export const config = {
  rxjsCentralKeys: {
    GridResize: 'gridResize',
    GridReady: 'gridReady',
    ShowFilterEsteiraDashBoard: 'showFilterEsteiraDashBoard',
    ShowFilterEsteiraGD: 'ShowFilterEsteiraGD',
    ChangeToMobile: 'changeToMobile',
    ChangeToWeb: 'changeToWeb',
    onRegisterUserSucess: 'onRegisterUserSucess',
    onLoginUserSucess: 'onLoginUserSucess',
    pwaUpdateRequest: 'pwaUpdateRequest'
  },
}
