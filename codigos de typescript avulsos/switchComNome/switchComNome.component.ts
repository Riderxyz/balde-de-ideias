import {
  Component,
  OnInit,
  Input,
  forwardRef,
} from '@angular/core';
import { NG_VALUE_ACCESSOR, ControlValueAccessor } from '@angular/forms';
@Component({
  selector: 'switchComNome',
  templateUrl: './switchComNome.component.html',
  styleUrls: ['./switchComNome.component.scss'],
  providers: [
    {
      provide: NG_VALUE_ACCESSOR,
      multi: true,
      useExisting: forwardRef(() => switchComNomeComponent),
    },
  ],
})
export class SwitchComNomeComponent implements OnInit, ControlValueAccessor {
  @Input() disabled = false;
  private _value: string;
  onChange: any = () => {};
  onTouched: any = () => {};
  constructor() {}
  ngOnInit(): void {}
  public get value() {
    return this._value;
  }
  public set value(v) {
    this._value = v;
    this.onChange(this._value);
    this.onTouched();
  }
  writeValue(obj: any): void {
    this._value = obj;
  }
  registerOnChange(fn: any): void {
    this.onChange = fn;
  }
  registerOnTouched(fn: any): void {
    this.onTouched = fn;
  }
}
