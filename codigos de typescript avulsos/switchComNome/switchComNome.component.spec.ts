import { ComponentFixture, TestBed } from '@angular/core/testing';

import { SwitchComNomeComponent } from './switchComNome.component';

describe('SinafToggleComponent', () => {
  let component: SwitchComNomeComponent;
  let fixture: ComponentFixture<SwitchComNomeComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ SwitchComNomeComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(SwitchComNomeComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
