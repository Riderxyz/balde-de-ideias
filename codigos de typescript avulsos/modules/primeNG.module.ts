import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

// Botões
import { CheckboxModule } from 'primeng/checkbox';
import { DropdownModule } from 'primeng/dropdown';
import { ButtonModule } from 'primeng/button';

// card e paineis
import { CardModule } from 'primeng/card';
import { ToolbarModule } from 'primeng/toolbar';
import { TabMenuModule } from 'primeng/tabmenu';
import { PanelModule } from 'primeng/panel';
import { AccordionModule } from 'primeng/accordion';

// Forms e inputs
import { AutoCompleteModule } from 'primeng/autocomplete';
import { CalendarModule } from 'primeng/calendar';
import { InputMaskModule } from 'primeng/inputmask';
import { ListboxModule } from 'primeng/listbox';
import { InputTextModule } from 'primeng/inputtext';

// Menus
import { SlideMenuModule } from 'primeng/slidemenu';
import { PanelMenuModule } from 'primeng/panelmenu';
import { MenuModule } from 'primeng/menu';
import { SplitButtonModule } from 'primeng/splitbutton';

// Dialogs e overlays
import { SidebarModule } from 'primeng/sidebar';
import { DialogModule } from 'primeng/dialog';
import { MessagesModule } from 'primeng/messages';
import { MessageModule } from 'primeng/message';
import { ToastModule } from 'primeng/toast';
import { ConfirmDialogModule } from 'primeng/confirmdialog';

// slide e efeitos

import { SliderModule } from 'primeng/slider';
import { SpinnerModule } from 'primeng/spinner';

@NgModule({
  imports: [
    CommonModule,
    // Botões
    CheckboxModule,
    DropdownModule,
    ButtonModule,
    // card e Toolbar,
    CardModule,
    ToolbarModule,
    TabMenuModule,
    PanelModule,
    AccordionModule,
    // Forms e inputs,
    AutoCompleteModule,
    CalendarModule,
    InputMaskModule,
    ListboxModule,
    InputTextModule,
    // Menus,
    SlideMenuModule,
    PanelMenuModule,
    MenuModule,
    SplitButtonModule,
    // Dialogs e overlays,
    SidebarModule,
    DialogModule,
    MessagesModule,
    MessageModule,
    ToastModule,
    ConfirmDialogModule,
    // slide e efeitos,
    SliderModule,
    SpinnerModule,
  ],
  declarations: [

  ],
  providers: [

  ],
  exports: [
    CommonModule,
    // Botões
    CheckboxModule,
    DropdownModule,
    ButtonModule,
    // card e Toolbar,
    CardModule,
    ToolbarModule,
    TabMenuModule,
    PanelModule,
    AccordionModule,
    // Forms e inputs,
    AutoCompleteModule,
    CalendarModule,
    InputMaskModule,
    ListboxModule,
    InputTextModule,
    // Menus,
    SlideMenuModule,
    PanelMenuModule,
    MenuModule,
    SplitButtonModule,
    // Dialogs e overlays,
    SidebarModule,
    DialogModule,
    MessagesModule,
    MessageModule,
    ToastModule,
    ConfirmDialogModule,
    // slide e efeitos,
    SliderModule,
    SpinnerModule,
  ]
})
export class PrimeNGModule { }
