import { NgModule, ModuleWithProviders } from '@angular/core';
import { CommonModule } from '@angular/common';
import {
    NbActionsModule,
    NbCardModule,
    NbLayoutModule,
    NbMenuModule,
    NbSelectModule,
    NbRouteTabsetModule,
    NbSearchModule,
    NbSidebarModule,
    NbTabsetModule,
    NbThemeModule,
    NbUserModule,
    NbButtonModule,
    NbCalendarModule,
    NbAlertModule,
    NbCheckboxModule,
    NbAccordionModule,
    NbStepperModule,
    NbInputModule,
    NbToastrModule,
    NbWindowModule,
    NbDialogModule,
    NbListModule,
    NbTooltipModule,
    NbDatepickerModule,
    NbPopoverModule
} from '@nebular/theme';
import { DEFAULT_THEME } from './theme.default';
import { COSMIC_THEME } from './theme.cosmic';
import { CORPORATE_THEME } from './theme.corporate';


const NB_THEME_PROVIDERS = [
    ...NbThemeModule.forRoot(
      {
        name: 'cosmic',
      },
      [ DEFAULT_THEME, COSMIC_THEME, CORPORATE_THEME ]
    ).providers,
    ...NbSidebarModule.forRoot().providers,
    ...NbMenuModule.forRoot().providers,
    ...NbDialogModule.forRoot().providers,
    ...NbWindowModule.forRoot().providers,
    ...NbDatepickerModule.forRoot().providers,
    ...NbToastrModule.forRoot().providers,
  ];



@NgModule({
    imports: [
        CommonModule,
        NbActionsModule,
        NbCardModule,
        NbLayoutModule,
        NbMenuModule,
        NbSelectModule,
        NbRouteTabsetModule,
        NbSearchModule,
        NbSidebarModule,
        NbTabsetModule,
        NbThemeModule,
        NbUserModule,
        NbButtonModule,
        NbCalendarModule,
        NbAlertModule,
        NbCheckboxModule,
        NbAccordionModule,
        NbStepperModule,
        NbInputModule,
        NbToastrModule,
        NbWindowModule,
        NbDialogModule,
        NbListModule,
        NbTooltipModule,
        NbDatepickerModule,
        NbPopoverModule
    ],
    declarations: [],
    exports: [
        NbActionsModule,
        NbCardModule,
        NbLayoutModule,
        NbMenuModule,
        NbSelectModule,
        NbRouteTabsetModule,
        NbSearchModule,
        NbSidebarModule,
        NbTabsetModule,
        NbThemeModule,
        NbUserModule,
        NbButtonModule,
        NbCalendarModule,
        NbAlertModule,
        NbCheckboxModule,
        NbAccordionModule,
        NbStepperModule,
        NbInputModule,
        NbToastrModule,
        NbWindowModule,
        NbDialogModule,
        NbListModule,
        NbTooltipModule,
        NbDatepickerModule,
        NbPopoverModule
    ]
})

export class NebularModule {
    static forRoot(): ModuleWithProviders {
        return {
          ngModule: NebularModule,
          providers: [...NB_THEME_PROVIDERS],
        } as ModuleWithProviders;
      }
}
