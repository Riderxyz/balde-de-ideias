import { Injectable } from '@angular/core';

@Injectable()
export class RandomService {

    allArrays = [
        ['primeiro Array'],
        ['segundo Array'],
        ['terceiro Array']
    ]
    constructor() { }
    /**
     * 
     * @param arr 
     * @type array de arrays. Não suporta array de objetos>
     */
    allPossibleCases(arr) {
        if (arr.length === 1) {
            return arr[0];
        } else {
            var result = [];
            var allCasesOfRest = this.allPossibleCases(arr.slice(1));  // recur with the rest of array
            for (var i = 0; i < allCasesOfRest.length; i++) {
                for (var j = 0; j < arr[0].length; j++) {
                    result.push(arr[0][j] + allCasesOfRest[i]);
                }
            }
            return result;
        }

    }

    getRandomId()  {
        return Math.random()
          .toString(36)
          .substr(2, 9);
      }

}