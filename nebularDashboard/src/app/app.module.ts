import { BrowserModule } from '@angular/platform-browser';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { LayoutModule } from '@angular/cdk/layout';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { HttpClientModule } from '@angular/common/http';
import { Location, LocationStrategy, PathLocationStrategy } from '@angular/common';
import { RoutingModule } from './app.routing';
import { AppComponent } from './app.component';
import { ServiceWorkerModule } from '@angular/service-worker';
import { NebularModule } from './Modules/nebular.module';

// Localidade
import { registerLocaleData } from '@angular/common';
import { LOCALE_ID } from '@angular/core';
import localePT from '@angular/common/locales/pt';

// AngularFire
import { environment } from './../environments/environment';
import { AngularFireModule } from '@angular/fire';
import { AngularFireDatabaseModule } from '@angular/fire/database';
import { AngularFireAuthModule } from '@angular/fire/auth';
import { AngularFireMessagingModule } from '@angular/fire/messaging';
import { AngularFireStorageModule } from '@angular/fire/storage';
import { DashboardComponent } from './pages/dashboard/dashboard.component';
import { ComponentModule } from './components/component.module';

registerLocaleData(localePT, 'pt');

const AngularFire = [
  AngularFireModule.initializeApp(environment.firebase),
  AngularFireDatabaseModule,
  AngularFireAuthModule,
  AngularFireMessagingModule,
  AngularFireStorageModule
]

const FormModules = [
  ReactiveFormsModule,
  FormsModule
]


@NgModule({
  declarations: [
    AppComponent,
    DashboardComponent
  ],
  imports: [
    BrowserModule,
    RoutingModule,
    HttpClientModule,
    ...FormModules,
    ComponentModule,
    // CDK
    BrowserAnimationsModule,
    LayoutModule,
    // AngularFire
    ...AngularFire,
    NebularModule.forRoot(),
    AngularFire,
    // PWA
    ServiceWorkerModule.register('combined-sw.js', { enabled: environment.production })
  ],
  providers: [
    { provide: LOCALE_ID, useValue: 'pt' },
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
