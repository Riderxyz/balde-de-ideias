import { Component, OnDestroy, OnInit } from '@angular/core';
import { NbThemeService } from '@nebular/theme';
import { takeWhile } from 'rxjs/operators';
import { forkJoin, Observable } from 'rxjs';


interface Temperature {
  value: number;
  min: number;
  max: number;
}

@Component({
  selector: 'ngx-temperature',
  styleUrls: ['./temperature.component.scss'],
  templateUrl: './temperature.component.html',
})
export class TemperatureComponent implements OnInit,OnDestroy {

  private alive = true;

  temperatureData: Temperature;
  temperature: number;
  temperatureOff = false;
  temperatureMode = 'cool';

  humidityData: Temperature;
  humidity: number;
  humidityOff = false;
  humidityMode = 'heat';

  colors: any;
  themeSubscription: any;
  private temperatureDate: Temperature = {
    value: 24,
    min: 12,
    max: 30,
  };

  private humidityDate: Temperature = {
    value: 87,
    min: 0,
    max: 100,
  };
  constructor(private theme: NbThemeService) {
    this.theme.getJsTheme()
      .pipe(takeWhile(() => this.alive))
      .subscribe(config => {
        console.log('Corres',config);

      this.colors = config.variables;
    });

 /*    forkJoin(
      this.getTemperatureData(),
      this.getHumidityData(),
    )
      .subscribe(([temperatureData, humidityData]: [Temperature, Temperature]) => {
        this.temperatureData = temperatureData;
        this.temperature = this.temperatureData.value;

        this.humidityData = humidityData;
        this.humidity = this.humidityData.value;
      }); */
  }
ngOnInit() {
  this.temperatureData = this.temperatureDate;
  this.temperature =  this.temperatureDate.value;

  this.humidityData = this.humidityDate;
  this.humidity = this.humidityDate.value;
}


  transform(input: number): number {
    return Math.round(input);
  }


  changeMode(ev) {
    console.log(Math.round(ev));

  }

  ngOnDestroy() {
    this.alive = false;
  }
}
