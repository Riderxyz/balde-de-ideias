import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { StatusCardComponent } from './status-card/status-card.component';
import { NebularModule } from '../Modules/nebular.module';
import { TemperatureComponent } from './temperature/temperature.component';
import {TemperatureDraggerComponent} from './temperature/temperature-dragger/temperature-dragger.component'
import { ReactiveFormsModule, FormsModule } from '@angular/forms';

const FormModules = [
  ReactiveFormsModule,
  FormsModule
]



const temperature = [
  TemperatureDraggerComponent,
  TemperatureComponent
]
@NgModule({
  declarations: [
    StatusCardComponent,
    ...temperature
  ],
  imports: [
    CommonModule,
  NebularModule,
  ...FormModules
 ],
  exports: [
    StatusCardComponent,
    ...temperature
  ],
  providers: [],
})
export class ComponentModule {}
