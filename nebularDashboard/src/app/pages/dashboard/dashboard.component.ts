import { Component, OnInit } from '@angular/core';
import { NbThemeService } from '@nebular/theme';
import { takeWhile } from 'rxjs/operators';

interface CardSettings {
  title: string;
  iconClass: string;
  type: string;
}

@Component({
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.scss']
})
export class DashboardComponent implements OnInit {
/*   statusCard: CardSettings[] =
    [{
      title: 'Light',
      iconClass: 'nb-lightbulb',
      type: 'primary',
    },
    {
      title: 'Roller Shades',
      iconClass: 'nb-roller-shades',
      type: 'success',
    }, {
      title: 'Wireless Audio',
      iconClass: 'nb-audio',
      type: 'info',
    }, {
      title: 'Coffee Maker',
      iconClass: 'nb-coffee-maker',
      type: 'warning',
    }
    ]; */
    private alive = true;
    lightCard: CardSettings = {
      title: 'Light',
      iconClass: 'nb-lightbulb',
      type: 'primary',
    };
    rollerShadesCard: CardSettings = {
      title: 'Roller Shades',
      iconClass: 'nb-roller-shades',
      type: 'success',
    };
    wirelessAudioCard: CardSettings = {
      title: 'Wireless Audio',
      iconClass: 'nb-audio',
      type: 'info',
    };
    coffeeMakerCard: CardSettings = {
      title: 'Coffee Maker',
      iconClass: 'nb-coffee-maker',
      type: 'warning',
    };
    commonStatusCardsSet: CardSettings[] = [
      this.lightCard,
      this.rollerShadesCard,
      this.wirelessAudioCard,
      this.coffeeMakerCard,
    ];
    statusCards: string;
    statusCardsByThemes: {
      default: CardSettings[];
      cosmic: CardSettings[];
      corporate: CardSettings[];
    } = {
      default: this.commonStatusCardsSet,
      cosmic: this.commonStatusCardsSet,
      corporate: [
        {
          ...this.lightCard,
          type: 'warning',
        },
        {
          ...this.rollerShadesCard,
          type: 'primary',
        },
        {
          ...this.wirelessAudioCard,
          type: 'danger',
        },
        {
          ...this.coffeeMakerCard,
          type: 'secondary',
        },
      ],
    };
    constructor(
    private themeService: NbThemeService,
  ) {


   }

  ngOnInit() {
    this.themeService.getJsTheme()
    .pipe(takeWhile(() => this.alive))
    .subscribe(theme => {
      console.log(theme.name);
      console.log(this.statusCardsByThemes[theme.name]);
      this.statusCards = this.statusCardsByThemes[theme.name];
  });
  }

}
