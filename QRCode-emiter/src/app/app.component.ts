import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent implements OnInit {
  title = 'QRCode-emiter';
  data = {
    client: 'Iago Favilla',
    table: '12345',
    orders: [],
    observation:''
  }
  jsonTest = '';
  constructor() {

  }
  ngOnInit() {
    this.jsonTest = JSON.stringify(this.data);
  console.log(this.jsonTest)
    }
}
