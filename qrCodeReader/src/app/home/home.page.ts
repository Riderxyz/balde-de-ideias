import { Component } from '@angular/core';
import {
  BarcodeScannerOptions,
  BarcodeScanner
} from "@ionic-native/barcode-scanner/ngx";
import * as JWT from 'jwt-decode';
@Component({
  selector: 'app-home',
  templateUrl: 'home.page.html',
  styleUrls: ['home.page.scss'],
})
export class HomePage {
  scannedData:any = '';
  ano_mes_dia = '';
  hora_minutos = '';
  Date = '';
  constructor(private barcodeScanner: BarcodeScanner) {}
  scanCode() {
    this.barcodeScanner
      .scan()
      .then(barcodeData => {
        console.log("Barcode data", barcodeData);
        this.scannedData = barcodeData.text;
       console.log(
         JWT(this.scannedData)         
        )

      })
      .catch(err => {
        console.log("Error", err);
      });
  }
  ionBlurToDate(ev) {
    console.log(ev);
    const ano = new Date(this.ano_mes_dia).getFullYear();
    const mes = new Date(this.ano_mes_dia).getMonth();
    const dia = new Date(this.ano_mes_dia).getDate();
    const hour = new Date(this.hora_minutos).getHours();
    const min = new Date(this.hora_minutos).getMinutes();
    this.Date = '' + ano + '-' + (mes + 1) + '-' + dia + ' ' + hour + ':' + min + ':00'
    
  }
}
