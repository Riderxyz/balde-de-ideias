import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'app-stats-card',
  templateUrl: './stats-card.component.html',
  styleUrls: ['./stats-card.component.scss']
})
export class StatsCardComponent implements OnInit {
  cardArr:{icon: string;
    name: string;
    value: string;
    progressBar: string;
    class: string;}[] = []

    @Input() statusArr: any[];
  constructor() { }

  ngOnInit() {
    this.cardArr = [
      {
        icon: 'far fa-money-bill-alt',
        name: 'SALES',
        value: 'R$2000',
        progressBar: '25',
        class: 'primary-color'
      },
      {
        icon: 'fas fa-chart-line',
        name: 'SUBSCRIPTIONS',
        value: '200',
        progressBar: '25',
        class: 'warning-color'
      },
      {
        icon: 'fas fa-chart-pie',
        name: 'TRAFFIC',
        value: '20000',
        progressBar: '75',
        class: 'light-blue lighten-1'
      },
      {
        icon: 'far fa-chart-bar',
        name: 'ORGANIC TRAFFIC',
        value: '2000',
        progressBar: '55',
        class: 'red accent-2'
      },
    ]
  }



}
