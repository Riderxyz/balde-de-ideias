// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false,
  firebase:{
    apiKey: "AIzaSyD43YJx5Ztgn3Fq3qCxiV-IQLQbO48MW4s",
    authDomain: "marketplace-226d4.firebaseapp.com",
    databaseURL: "https://marketplace-226d4.firebaseio.com",
    projectId: "marketplace-226d4",
    storageBucket: "marketplace-226d4.appspot.com",
    messagingSenderId: "561500806699",
    appId: "1:561500806699:web:baac09ab3f7b52fc09546e",
    measurementId: "G-34J2696PH6"
  }
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
