import { Component, OnInit } from '@angular/core';
import { ParticleInterface } from 'src/interfaces/particlesJS.interface';

@Component({
  selector: 'app-welcome-banner',
  templateUrl: './welcome-banner.component.html',
  styleUrls: ['./welcome-banner.component.scss']
})
export class WelcomeBannerComponent implements OnInit {
  WelcomeBannerParticlesStyle = {};
  WelcomeBannerParams: ParticleInterface = {};
  width = 100;
  height = 100;
  constructor() { }

  ngOnInit() {
    this.WelcomeBannerParticlesStyle = {
      'background-color': '#001031',
      'background-size': 'cover',
      top: 0,
      left: 0,
      width: '100%',
      height: '400px'
    };

    this.WelcomeBannerParams = {
      particles: {
        number: {
          value: 400
        },
        color: {
          value: '#00ffff'
        },
        opacity: {
          value: 0.5
        },
        line_linked: {
          enable: false,
        },
        size: {
          value: 5,
          random: true,
          anim: {
            enable: false,
            speed: 80,
            size_min: 0.1,
            sync: false
          }
        },
        move: {
          enable: true,
          speed: 2
        }
      },
      interactivity:{
        detect_on: 'canvas',
        events:{
          onhover:{
            enable: false
          },
          onclick:{
            enable: false
          },
          resize: true
        },

      }
    };
  }

}
