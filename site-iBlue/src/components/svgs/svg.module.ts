import { NgModule } from "@angular/core";
import { DeploySvgComponent } from "./homePage_buttons/deploy/deploySvg.component";
import { RunSvgComponent } from "./homePage_buttons/run/runSvg.component";
import { UpdateSvgComponent } from "./homePage_buttons/update/updateSvg.component";
import { CommonModule } from '@angular/common';

const homePage_buttons = [
  DeploySvgComponent,
  RunSvgComponent,
  UpdateSvgComponent
];
@NgModule({
  imports: [CommonModule],
  exports: [...homePage_buttons],
  declarations: [...homePage_buttons],
  providers: []
})
export class SvgModule {}
