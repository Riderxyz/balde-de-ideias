import { Component, OnInit, HostListener } from '@angular/core';

@Component({
  selector: 'app-updateSvg',
  templateUrl: 'updateSvg.component.html',
  styleUrls: ['./updateSvg.component.scss']
})

export class UpdateSvgComponent implements OnInit {
  onHover = false;
  @HostListener('mouseenter') onMouseEnter() {
    this.onHover = true;
  }
  @HostListener('mouseleave') onMouseLeave() {
    this.onHover = false;
  }
  constructor() { }

  ngOnInit() { }
}
