import { Component, OnInit, HostListener } from "@angular/core";

@Component({
  selector: "app-deploySvg",
  templateUrl: "deploySvg.component.html",
  styleUrls: ["./deploySvg.component.scss"]
})
export class DeploySvgComponent implements OnInit {
  onHover = false;
  @HostListener('mouseenter') onMouseEnter() {
    this.onHover = true;
  }
  @HostListener('mouseleave') onMouseLeave() {
    this.onHover = false;
  }
  constructor() {}

  ngOnInit() {}
}
