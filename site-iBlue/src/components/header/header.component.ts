import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.scss']
})
export class HeaderComponent implements OnInit {
menuItems = [
  {
    name: 'Cases',
    route: ''
  },
  {
    name: 'Parcerias',
    route: ''
  },
  {
    name: 'Liderança',
    route: ''
  },
  {
    name: 'Contatos',
    route: ''
  }
]
dropdowns = [
  {
    name: 'iBlue.Update '
  },
  {
    name: 'iBlue.Run '
  },
  {
    name: 'iBlue.Deploy '
  },
]
  constructor() { }

  ngOnInit() {
  }

}
