import { Component, OnInit } from '@angular/core';
import { ParticleInterface } from 'src/interfaces/particlesJS.interface';
import { CasesInterface } from 'src/interfaces/cases.interface';


@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss']
})
export class HomeComponent implements OnInit {
  cases: CasesInterface[] = [];
  onHover = false;
  constructor() { }
  ngOnInit() {
    this.cases = [
      {
        id: 'ipiranga',
        title: 'Ipiranga Especialistas',
        subtitle: 'Instagram Corporativo',
        texto: 'Buscando aumentar o engajamento na sua força de venda, a Ipiranga, buscou a iBlue para a criação de uma solução criativa que conseguisse atingir esse objetivo.',
        image: '../../assets/cases/BTN_IPIRANGA_NORMAL.png',
        imageHover: '../../assets/cases/BTN_IPIRANGA_HOVER.svg',
        showCard: false,
        showImg: true
      },
      {
        id: 'generali',
        title: 'Generali',
        subtitle: 'Novo produto Smart Auto',
        texto: 'Uma inovação no mercado de seguros de automóveis, o Smart Auto. O desafio era sustentar e evoluir o aplicativo mobile e construir o Backend do produto.',
        image: '../../assets/cases/BTN_GENERALI_NORMAL.png',
        imageHover: '../../assets/cases/BTN_GENERALI_HOVER.svg',
        showCard: false,
        showImg: true
      },
      {
        id: 'brasilcap',
        title: 'Brasilcap',
        subtitle: 'Nova plataforma de negócios',
        texto: 'Um dos maiores e mais ambiciosos projetos da BrasilCap, reconstruir toda a sua plataforma principal de negócios.',
        image: '../../assets/cases/BTN_BRASILCAP_NORMAL.png',
        imageHover: '../../assets/cases/BTN_BRASILCAP_HOVER.svg',
        showCard: false,
        showImg: true
      },
      {
        id: 'azul',
        title: 'Azul Seguros',
        subtitle: 'Transformação Digital',
        texto: 'Implementação do Salesforce na empresa, além do desenvolvimento de motores de inteligência para o chatbot das áreas de sinistro.',
        image: '../../assets/cases/BTN_AZUL_NORMAL.png',
        imageHover: '../../assets/cases/BTN_AZUL_HOVER.svg',
        showCard: false,
        showImg: true
      },
      {
        id: 'mongeral',
        title: 'Mongeral',
        subtitle: 'Sustentação Ágil do Sistema Mongeral/Sicoob',
        texto: 'A iblue ficou responsável de implementar o sistema de seguros da Mongeral Aegon dentro do Sicoob, maior sistema financeiro cooperativo do país.',
        image: '../../assets/cases/BTN_MONGERAL_NORMAL.png',
        imageHover: '../../assets/cases/BTN_MONGERAL_HOVER.svg',
        showCard: false,
        showImg: true
      },
      {
        id: 'ciclic',
        title: 'Ciclic',
        subtitle: 'Co-criação de novos produtos',
        texto: 'Um dos maiores desafios propostos para a iblue, co-criar juntamente com a Ciclic um produto totalmente novo para a empresa em tempo record, menos de 40 dias.',
        image: '../../assets/cases/BTN_CICLIC_NORMAL.png',
        imageHover: '../../assets/cases/BTN_CICLIC_HOVER.svg',
        showCard: false,
        showImg: true
      }
    ]
  }


}
