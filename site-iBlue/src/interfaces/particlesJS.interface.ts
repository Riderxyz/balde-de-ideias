export interface ParticleInterface {
  particles?: Particles;
  interactivity?: Interactivity;
  retina_detect?: boolean;
}

interface Interactivity {
  detect_on: string;
  events: Events;
  modes?: Modes;
}

interface Modes {
  grab: Grab;
  bubble: Bubble;
  repulse: Repulse;
  push: Push;
  remove: Push;
}

interface Push {
  particles_nb: number;
}

interface Repulse {
  distance: number;
  duration: number;
}

interface Bubble {
  distance: number;
  size: number;
  duration: number;
  opacity: number;
  speed: number;
}

interface Grab {
  distance: number;
  line_linked: Linelinked2;
}

interface Linelinked2 {
  opacity: number;
}

interface Events {
  onhover: Onhover;
  onclick: Onhover;
  resize: boolean;
}

interface Onhover {
  enable: boolean;
  mode?: string;
}

interface Particles {
  number: NumberObj;
  color?: Color;
  shape?: Shape;
  opacity?: Opacity;
  size?: Size;
  line_linked?: Linelinked;
  move?: Move;
}

interface Move {
  enable: boolean;
  speed?: number;
  direction?: string;
  random?: boolean;
  straight?: boolean;
  out_mode?: string;
  bounce?: boolean;
  attract?: Attract;
}

interface Attract {
  enable: boolean;
  rotateX: number;
  rotateY: number;
}

interface Linelinked {
  enable: boolean;
  distance?: number;
  color?: string;
  opacity?: number;
  width?: number;
}

interface Size {
  value: number;
  random: boolean;
  anim: Anim2;
}

interface Anim2 {
  enable: boolean;
  speed: number;
  size_min: number;
  sync: boolean;
}

interface Opacity {
  value: number;
  random?: boolean;
  anim?: Anim;
}

interface Anim {
  enable: boolean;
  speed: number;
  opacity_min: number;
  sync: boolean;
}

interface Shape {
  type: string;
  stroke: Stroke;
  polygon: Polygon;
  image: Image;
}

interface Image {
  src: string;
  width: number;
  height: number;
}

interface Polygon {
  nb_sides: number;
}

interface Stroke {
  width: number;
  color: string;
}

interface Color {
  value: string;
}

interface NumberObj {
  value: number;
  density?: Density;
}

interface Density {
  enable: boolean;
  value_area: number;
}
