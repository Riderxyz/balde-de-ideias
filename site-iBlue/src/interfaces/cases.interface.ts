export interface CasesInterface {
  id: string;
  title: string;
  subtitle: string;
  texto: string;
  image: string;
  imageHover: string;
  showCard: boolean;
  showImg: boolean;
}
