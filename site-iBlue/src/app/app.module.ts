import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { HttpClientModule } from '@angular/common/http';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';


import { MDBBootstrapModule } from 'angular-bootstrap-md';
import { AppRoutingModule } from './app.routing';
import { AppComponent } from './app.component';
import { HeaderComponent } from '../components/header/header.component';
import { HomeComponent } from '../pages/home/home.component';

import { ParticlesModule } from 'angular-particle';
import { WelcomeBannerComponent } from 'src/components/welcomeBanner/welcome-banner.component';
import { SvgModule } from 'src/components/svgs/svg.module';

const components = [
  HeaderComponent,
  WelcomeBannerComponent
]
@NgModule({
  declarations: [
    AppComponent,
    ...components,
    HomeComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    SvgModule,
    MDBBootstrapModule.forRoot(),
    ReactiveFormsModule,
    HttpClientModule,
    ParticlesModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
